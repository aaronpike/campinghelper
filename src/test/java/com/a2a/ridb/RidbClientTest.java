package com.a2a.ridb;

import com.a2a.ridb.models.RecData;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Aaron
 */
public class RidbClientTest {
    
    public RidbClientTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getFacilities method, of class RidbClient.
     */
    @Test
    public void testGetFacilities() {	
	RidbClient instance = new RidbClient();
	RecData facilities = instance.getFacilities("40.7500", "111.8833", "50");
	
	Assert.assertNotNull(facilities);
    }
    
}
