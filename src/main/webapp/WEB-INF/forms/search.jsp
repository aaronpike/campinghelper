<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<header>
    <h2>Camp Finder</h2>
</header>
<p>Enter a Zip Code, City, or State to find camping areas.</p>

<form:form id="searchForm" method="GET" action="search.do" modelAttribute="search">
    <div>
	<label for="city">City: </label>
	<form:input path="city" />
    </div>

    <div>
	<label for="state">State: </label>
	<form:input path="state" />
    </div>

    <div>
	<label for="zipCode">Zip Code: </label>
	<form:input path="zipCode" />
    </div>
</form:form>

<footer>
    <ul class="buttons vertical">
	<li><a href="#" onclick="$('#searchForm').submit();" class="button fit scrolly">Search</a></li>
    </ul>
</footer>