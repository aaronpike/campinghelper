<%@tag description="Scripts" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!--[if lte IE 8]><script src="<c:url value="/twenty/css/ie/html5shiv.js" />"></script><![endif]-->
<script src="<c:url value="/twenty/js/jquery.min.js" />"></script>
<script src="<c:url value="/twenty/js/jquery.dropotron.min.js" />"></script>
<script src="<c:url value="/twenty/js/jquery.scrolly.min.js" />"></script>
<script src="<c:url value="/twenty/js/jquery.scrollgress.min.js" />"></script>
<script src="<c:url value="/twenty/js/skel.min.js" />"></script>
<script src="<c:url value="/twenty/js/skel-layers.min.js" />"></script>
<script src="<c:url value="/twenty/js/init.js" />"></script>