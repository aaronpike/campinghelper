<%@tag description="Main Wrapper" pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
    <head>
	<t:head></t:head>
    </head>
    
    <body class="index">
        <jsp:doBody/>
    </body>
</html>