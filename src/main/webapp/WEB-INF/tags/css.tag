<%@tag description="CSS" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<noscript>
<link rel="stylesheet" href="<c:url value="/twenty/css/skel.css" />" />
<link rel="stylesheet" href="<c:url value="/twenty/css/style.css" />" />
<link rel="stylesheet" href="<c:url value="/twenty/css/style-wide.css" />" />
<link rel="stylesheet" href="<c:url value="/twenty/css/style-noscript.css" />" />
</noscript>
<!--[if lte IE 8]><link rel="stylesheet" href="<c:url value="/twenty/css/ie/v8.css" />" /><![endif]-->
<!--[if lte IE 9]><link rel="stylesheet" href="<c:url value="/twenty/css/ie/v9.css" />" /><![endif]-->