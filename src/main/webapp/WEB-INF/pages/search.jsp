<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
    <head>
	<title>Camping Helper</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<!--[if lte IE 8]><script src="<c:url value="/twenty/css/ie/html5shiv.js" />"></script><![endif]-->
	<script src="<c:url value="/twenty/js/jquery.min.js" />"></script>
	<script src="<c:url value="/twenty/js/jquery.dropotron.min.js" />"></script>
	<script src="<c:url value="/twenty/js/jquery.scrolly.min.js" />"></script>
	<script src="<c:url value="/twenty/js/jquery.scrollgress.min.js" />"></script>
	<script src="<c:url value="/twenty/js/skel.min.js" />"></script>
	<script src="<c:url value="/twenty/js/skel-layers.min.js" />"></script>
	<script src="<c:url value="/twenty/js/init.js" />"></script>
	<noscript>
	<link rel="stylesheet" href="<c:url value="/twenty/css/skel.css" />" />
	<link rel="stylesheet" href="<c:url value="/twenty/css/style.css" />" />
	<link rel="stylesheet" href="<c:url value="/twenty/css/style-wide.css" />" />
	<link rel="stylesheet" href="<c:url value="/twenty/css/style-noscript.css" />" />
	</noscript>
	<!--[if lte IE 8]><link rel="stylesheet" href="<c:url value="/twenty/css/ie/v8.css" />" /><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" href="<c:url value="/twenty/css/ie/v9.css" />" /><![endif]-->
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj9Kb5Ak2VBtLMG3XP6-Yig9PxPqz9uj4"></script>
	<script type="text/javascript">
	    var map;
	    var infowindow = new google.maps.InfoWindow({
		content: ''
	    });

	    google.maps.event.addDomListener(window, 'load', initialize);

	    function initialize() {
		var mapOptions = {
		    center: {lat: ${lat}, lng: ${lng}},
		    zoom: 8
		};

		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

		var timeout;
		google.maps.event.addListener(map, 'bounds_changed', function () {
		    window.clearTimeout(timeout);
		    timeout = window.setTimeout(function () {
			var radius = getViewableRadius();
			showRidb(radius);
		    }, 500);
		});
	    }

	    function showRidb(radius) {
		jQuery.ajax({
		    url: '<c:url value="/data/places.do" />',
		    data: { 
			"latitude": map.getCenter().lat(),
			"longitude": map.getCenter().lng(),
			"radius": radius
		    },
		    dataType: 'json',
		    success: function (response) {
			recData = response.RECDATA;

			// loop through places and add markers
			for (var key in recData) {
			    //create gmap latlng obj
			    tmpLatLng = new google.maps.LatLng(recData[key].FacilityLatitude, recData[key].FacilityLongitude);

			    // make and place map maker.
			    var marker = new google.maps.Marker({
				map: map,
				position: tmpLatLng,
				title: recData[key].FacilityName
			    });

			    bindInfoWindow(marker, map, infowindow, '<div style="color: #000;"><strong>' + recData[key].FacilityName + "</strong><br>" + recData[key].FacilityDescription + "</div>");
			}
		    }
		});
	    }

	    /**
	     * Get's the radius of viewable map
	     * 
	     * @returns {Number}
	     */
	    function getViewableRadius() {
		var bounds = map.getBounds();
		var center = bounds.getCenter();
		var ne = bounds.getNorthEast();

		// r = radius of the earth in statute miles
		var r = 3963.0;

		// Convert lat or lng from decimal degrees into radians (divide by 57.2958)
		var lat1 = center.lat() / 57.2958;
		var lon1 = center.lng() / 57.2958;
		var lat2 = ne.lat() / 57.2958;
		var lon2 = ne.lng() / 57.2958;

		// distance = circle radius from center to Northeast corner of bounds
		var dis = r * Math.acos(Math.sin(lat1) * Math.sin(lat2) +
			Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1));

		return dis;
	    }

	    // binds a map marker and infoWindow together on click
	    var bindInfoWindow = function (marker, map, infowindow, html) {
		console.log(html);
		google.maps.event.addListener(marker, 'click', function () {
		    infowindow.setContent(html);
		    infowindow.open(map, marker);
		});
	    }
	</script>
    </head>

    <body class="index">
	<header id="header" class="alt">
	    <h1 id="logo"><a href="/index.do">Camping Helper <span>A2A Studios</span></a></h1>
	    <nav id="nav">
		<ul>
		    <li class="current"><a href="index.html">Welcome</a></li>
		    <li class="submenu">
			<a href="">Menu</a>
			<ul>
			    <li><a href="#about">About</a></li>
			    <li><a href="#contact">Contact</a></li>
			    <li class="submenu">
				<a href="">States</a>
				<ul>
				    <c:forEach items="${states}" var="state">
					<li><a href="<c:url value="/search.do?state=${state.name}" />">${state.name}</a></li>
					</c:forEach>
				</ul>
			    </li>
			</ul>
		    </li>
		</ul>
	    </nav>
	</header>

	<section id="searchResults" class="main">
	    <header>
		<h2>Search Results</h2>
		<p>Click the dots for more information.</p>
	    </header>	    
	    <div id="map-canvas" class="inner"></div>
	</section>

	<!-- Footer -->
	<jsp:include page="/WEB-INF/tags/footer.tag" flush="true"/>
    </body>
</html>