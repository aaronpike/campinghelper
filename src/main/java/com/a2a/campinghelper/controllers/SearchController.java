
package com.a2a.campinghelper.controllers;

import com.a2a.campinghelper.dao.StateFacadeLocal;
import com.a2a.campinghelper.forms.Search;
import com.a2a.ridb.RidbClient;
import com.a2a.ridb.models.RecData;
import google.geocode.GoogleGeocode;
import google.geocode.model.GeocodeResponse;
import java.io.IOException;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/** 
 *
 */
@Controller
public class SearchController {
    
    private final static Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
    
    @Resource
    private StateFacadeLocal stateFacade;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String onSubmit(@ModelAttribute("search") Search search, ModelMap model) {
	model.addAttribute("states", stateFacade.findAll());
	
	String address = getAddress(search);
	
	try {
	    GoogleGeocode googleGeocode = new GoogleGeocode(address);	
	    GeocodeResponse geocodeResponse = googleGeocode.getResponseObject();
	    model
		.addAttribute("lat", geocodeResponse.getGeometry().getLocation().getLat())
		.addAttribute("lng", geocodeResponse.getGeometry().getLocation().getLng());
	} catch(IOException ex) {
	    LOGGER.error("Unable to decode: " + ex.getMessage());
	}

	return "search";
    }
    
    @RequestMapping(value = "/data/places")
    public @ResponseBody RecData getCampingLocations(@RequestParam("latitude") String latitude, @RequestParam("longitude") String longitude, @RequestParam("radius") String radius) {	
	RidbClient ridbClient = new RidbClient();
	return ridbClient.getFacilities(latitude, longitude, radius);	
    }
    
    /**
     * Build Address
     * 
     * @param search
     * @return 
     */
    private String getAddress(Search search) {
	
	StringBuilder address = new StringBuilder();
	
	if (!StringUtils.isEmpty(search.getCity())) {
	    address.append(search.getCity());
	    address.append(" ");
	}
	
	if (!StringUtils.isEmpty(search.getState())) {
	    address.append(search.getState());
	    address.append(" ");
	}
	
	if (!StringUtils.isEmpty(search.getZipCode())) {
	    address.append(search.getZipCode());
	    address.append(" ");
	}
	    
	return address.toString();
    }

}