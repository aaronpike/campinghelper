package com.a2a.campinghelper.controllers;

import com.a2a.campinghelper.dao.StateFacadeLocal;
import com.a2a.campinghelper.forms.Search;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 */
@Controller("indexController")
public class IndexController {

    @Resource
    private StateFacadeLocal stateFacade;

    @RequestMapping(name = "/index", method = RequestMethod.GET)
    public String initializeForm(ModelMap model) {
	model.addAttribute("search", new Search());
	model.addAttribute("states", stateFacade.findAll());

	return "index";
    }
}
