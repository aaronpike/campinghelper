
package com.a2a.campinghelper.entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

/**
 *
 */
@AutoProperty
@Entity
@Table(name = "states", schema = "campinghelper")
@NamedQueries({
    @NamedQuery(name = "State.findAll", query = "SELECT s FROM State s"),
    @NamedQuery(name = "State.findByStateId", query = "SELECT s FROM State s WHERE s.stateId = :stateId"),
    @NamedQuery(name = "State.findByName", query = "SELECT s FROM State s WHERE s.name = :name"),
    @NamedQuery(name = "State.findByAbbreviation", query = "SELECT s FROM State s WHERE s.abbreviation = :abbreviation"),
    @NamedQuery(name = "State.findByCountry", query = "SELECT s FROM State s WHERE s.country = :country"),
    @NamedQuery(name = "State.findByType", query = "SELECT s FROM State s WHERE s.type = :type"),
    @NamedQuery(name = "State.findBySort", query = "SELECT s FROM State s WHERE s.sort = :sort"),
    @NamedQuery(name = "State.findByStatus", query = "SELECT s FROM State s WHERE s.status = :status"),
    @NamedQuery(name = "State.findByOccupied", query = "SELECT s FROM State s WHERE s.occupied = :occupied"),
    @NamedQuery(name = "State.findByNotes", query = "SELECT s FROM State s WHERE s.notes = :notes"),
    @NamedQuery(name = "State.findByFipsState", query = "SELECT s FROM State s WHERE s.fipsState = :fipsState"),
    @NamedQuery(name = "State.findByAssocPress", query = "SELECT s FROM State s WHERE s.assocPress = :assocPress"),
    @NamedQuery(name = "State.findByStandardFederalRegion", query = "SELECT s FROM State s WHERE s.standardFederalRegion = :standardFederalRegion"),
    @NamedQuery(name = "State.findByCensusRegion", query = "SELECT s FROM State s WHERE s.censusRegion = :censusRegion"),
    @NamedQuery(name = "State.findByCensusRegionName", query = "SELECT s FROM State s WHERE s.censusRegionName = :censusRegionName"),
    @NamedQuery(name = "State.findByCensusDivision", query = "SELECT s FROM State s WHERE s.censusDivision = :censusDivision"),
    @NamedQuery(name = "State.findByCensusDivisionName", query = "SELECT s FROM State s WHERE s.censusDivisionName = :censusDivisionName"),
    @NamedQuery(name = "State.findByCircuitCourt", query = "SELECT s FROM State s WHERE s.circuitCourt = :circuitCourt")})
public class State implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "state_id")
    private Integer stateId;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "abbreviation")
    private String abbreviation;
    @Size(max = 255)
    @Column(name = "country")
    private String country;
    @Size(max = 255)
    @Column(name = "type")
    private String type;
    @Column(name = "sort")
    private Integer sort;
    @Size(max = 255)
    @Column(name = "status")
    private String status;
    @Size(max = 255)
    @Column(name = "occupied")
    private String occupied;
    @Size(max = 255)
    @Column(name = "notes")
    private String notes;
    @Size(max = 255)
    @Column(name = "fips_state")
    private String fipsState;
    @Size(max = 255)
    @Column(name = "assoc_press")
    private String assocPress;
    @Size(max = 255)
    @Column(name = "standard_federal_region")
    private String standardFederalRegion;
    @Size(max = 255)
    @Column(name = "census_region")
    private String censusRegion;
    @Size(max = 255)
    @Column(name = "census_region_name")
    private String censusRegionName;
    @Size(max = 255)
    @Column(name = "census_division")
    private String censusDivision;
    @Size(max = 255)
    @Column(name = "census_division_name")
    private String censusDivisionName;
    @Size(max = 255)
    @Column(name = "circuit_court")
    private String circuitCourt;

    public State() {
    }

    public State(Integer stateId) {
	this.stateId = stateId;
    }

    public Integer getStateId() {
	return stateId;
    }

    public void setStateId(Integer stateId) {
	this.stateId = stateId;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getAbbreviation() {
	return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
	this.abbreviation = abbreviation;
    }

    public String getCountry() {
	return country;
    }

    public void setCountry(String country) {
	this.country = country;
    }

    public String getType() {
	return type;
    }

    public void setType(String type) {
	this.type = type;
    }

    public Integer getSort() {
	return sort;
    }

    public void setSort(Integer sort) {
	this.sort = sort;
    }

    public String getStatus() {
	return status;
    }

    public void setStatus(String status) {
	this.status = status;
    }

    public String getOccupied() {
	return occupied;
    }

    public void setOccupied(String occupied) {
	this.occupied = occupied;
    }

    public String getNotes() {
	return notes;
    }

    public void setNotes(String notes) {
	this.notes = notes;
    }

    public String getFipsState() {
	return fipsState;
    }

    public void setFipsState(String fipsState) {
	this.fipsState = fipsState;
    }

    public String getAssocPress() {
	return assocPress;
    }

    public void setAssocPress(String assocPress) {
	this.assocPress = assocPress;
    }

    public String getStandardFederalRegion() {
	return standardFederalRegion;
    }

    public void setStandardFederalRegion(String standardFederalRegion) {
	this.standardFederalRegion = standardFederalRegion;
    }

    public String getCensusRegion() {
	return censusRegion;
    }

    public void setCensusRegion(String censusRegion) {
	this.censusRegion = censusRegion;
    }

    public String getCensusRegionName() {
	return censusRegionName;
    }

    public void setCensusRegionName(String censusRegionName) {
	this.censusRegionName = censusRegionName;
    }

    public String getCensusDivision() {
	return censusDivision;
    }

    public void setCensusDivision(String censusDivision) {
	this.censusDivision = censusDivision;
    }

    public String getCensusDivisionName() {
	return censusDivisionName;
    }

    public void setCensusDivisionName(String censusDivisionName) {
	this.censusDivisionName = censusDivisionName;
    }

    public String getCircuitCourt() {
	return circuitCourt;
    }

    public void setCircuitCourt(String circuitCourt) {
	this.circuitCourt = circuitCourt;
    }

    @Override
    public int hashCode() {
	return Pojomatic.hashCode(this);
		
    }

    @Override
    public boolean equals(Object object) {
	return Pojomatic.equals(this, object);
    }

    @Override
    public String toString() {	
	return Pojomatic.toString(this);
    }

}
