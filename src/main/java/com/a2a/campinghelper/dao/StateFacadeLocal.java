package com.a2a.campinghelper.dao;

import com.a2a.campinghelper.entities.State;
import java.io.Serializable;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Aaron
 */
@Local
public interface StateFacadeLocal {

    void persist(State state);

    void delete(State state);

    State find(Serializable id);

    List<State> findAll();

    int count();
    
}
