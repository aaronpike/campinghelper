
package com.a2a.campinghelper.dao;

import java.io.Serializable;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 */
public abstract class AbstractFacade<T> {
    
    private final static Logger logger = LoggerFactory.getLogger(AbstractFacade.class);
    
    private Class<T> entityClass;    
     
    @Autowired
    private SessionFactory sessionFactory;
 
    protected Session getSession(){
        return sessionFactory.getCurrentSession();
    }
 
    public void persist(T entity) {
        getSession().persist(entity);
    }
     
    public void delete(T entity) {
        getSession().delete(entity);
    }

    public AbstractFacade(Class<T> entityClass) {
	this.entityClass = entityClass;
    }

    public T find(Serializable id) {
	return (T) getSession().get(entityClass, id);
    }
    
    public List<T> findAll() {
	List objects = null;
	try {
	    Query query = getSession().createQuery("from " + entityClass.getName());
	    objects = query.list();
	} catch (HibernateException e) {
	    logger.error(e.getMessage());
	}
	
	return objects;
    }

    public int count() {
	try {
	    Long l = (Long) getSession().createCriteria(entityClass).setProjection(Projections.rowCount()).uniqueResult();
	    return l.intValue();
	} catch (HibernateException e) {
	    logger.error(e.getMessage());
	    
	    return 0;
	}
    }

}
