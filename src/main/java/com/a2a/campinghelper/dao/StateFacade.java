
package com.a2a.campinghelper.dao;

import com.a2a.campinghelper.entities.State;
import javax.ejb.Stateless;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
@Stateless(mappedName="stateFacade")
public class StateFacade extends AbstractFacade<State> implements StateFacadeLocal {

    public StateFacade() {
	super(State.class);
    }

}
