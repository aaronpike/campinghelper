
package com.a2a.campinghelper.forms;

import java.io.Serializable;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

/**
 *
 */
@AutoProperty
public class Search implements Serializable {
    
    private String city;
    private String state;
    private String zipCode;

    /**
     * @return the city
     */
    public String getCity() {
	return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
	this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
	return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
	this.state = state;
    }

    /**
     * @return the zipCode
     */
    public String getZipCode() {
	return zipCode;
    }

    /**
     * @param zipCode the zipCode to set
     */
    public void setZipCode(String zipCode) {
	this.zipCode = zipCode;
    }

    @Override
    public int hashCode() {
	return Pojomatic.hashCode(this);
		
    }

    @Override
    public boolean equals(Object object) {
	return Pojomatic.equals(this, object);
    }

    @Override
    public String toString() {	
	return Pojomatic.toString(this);
    }

}
