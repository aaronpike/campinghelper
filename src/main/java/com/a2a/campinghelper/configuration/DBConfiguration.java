package com.a2a.campinghelper.configuration;

import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 *
 */
@Configuration
@EnableTransactionManagement
@ComponentScan({"com.a2a.campinghelper.configuration"})
public class DBConfiguration {

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
	LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
	sessionFactory.setDataSource(dataSource());
	sessionFactory.setPackagesToScan(new String[]{
	    "com.a2a.campinghelper.entities"
	});

	sessionFactory.setHibernateProperties(hibernateProperties());

	return sessionFactory;
    }

    @Bean
    public DataSource dataSource() {
	JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
	DataSource dataSource = dataSourceLookup.getDataSource("java:/mysql");
	return dataSource;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
	return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
	HibernateTransactionManager txManager = new HibernateTransactionManager();
	txManager.setSessionFactory(sessionFactory);

	return txManager;
    }

    Properties hibernateProperties() {
	Properties properties = new Properties();
	properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");

	return properties;
    }
}
