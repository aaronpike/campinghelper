
package com.a2a.ridb.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.pojomatic.Pojomatic;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "COORDINATES",
    "TYPE"
})
public class GeoCoordinates {

    @JsonProperty("COORDINATES")
    private List<Double> COORDINATES = new ArrayList<Double>();
    @JsonProperty("TYPE")
    private String TYPE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The COORDINATES
     */
    @JsonProperty("COORDINATES")
    public List<Double> getCOORDINATES() {
        return COORDINATES;
    }

    /**
     * 
     * @param COORDINATES
     *     The COORDINATES
     */
    @JsonProperty("COORDINATES")
    public void setCOORDINATES(List<Double> COORDINATES) {
        this.COORDINATES = COORDINATES;
    }

    public GeoCoordinates withCOORDINATES(List<Double> COORDINATES) {
        this.COORDINATES = COORDINATES;
        return this;
    }

    /**
     * 
     * @return
     *     The TYPE
     */
    @JsonProperty("TYPE")
    public String getTYPE() {
        return TYPE;
    }

    /**
     * 
     * @param TYPE
     *     The TYPE
     */
    @JsonProperty("TYPE")
    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public GeoCoordinates withTYPE(String TYPE) {
        this.TYPE = TYPE;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public GeoCoordinates withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
	return Pojomatic.hashCode(this);
		
    }

    @Override
    public boolean equals(Object object) {
	return Pojomatic.equals(this, object);
    }

    @Override
    public String toString() {	
	return Pojomatic.toString(this);
    }

}
