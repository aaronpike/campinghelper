package com.a2a.ridb.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.pojomatic.Pojomatic;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "RecAreaMapURL",
    "LastUpdatedBy",
    "RecAreaReservationURL",
    "RecAreaFeeDescription",
    "RecAreaName",
    "RecAreaDescription",
    "Keywords",
    "RecAreaEmail",
    "RecAreaLatitude",
    "StayLimit",
    "GEOJSON",
    "LastUpdatedDate",
    "RecAreaID",
    "RecAreaLongitude",
    "RecAreaDirections",
    "RecAreaPhone",
    "OrgRecAreaID"
})
public class RecreationArea {

    @JsonProperty("RecAreaMapURL")
    private String RecAreaMapURL;
    @JsonProperty("LastUpdatedBy")
    private Integer LastUpdatedBy;
    @JsonProperty("RecAreaReservationURL")
    private String RecAreaReservationURL;
    @JsonProperty("RecAreaFeeDescription")
    private String RecAreaFeeDescription;
    @JsonProperty("RecAreaName")
    private String RecAreaName;
    @JsonProperty("RecAreaDescription")
    private String RecAreaDescription;
    @JsonProperty("Keywords")
    private String Keywords;
    @JsonProperty("RecAreaEmail")
    private String RecAreaEmail;
    @JsonProperty("RecAreaLatitude")
    private Double RecAreaLatitude;
    @JsonProperty("StayLimit")
    private String StayLimit;
    @JsonProperty("GEOJSON")
    private com.a2a.ridb.models.GeoCoordinates GEOJSON;
    @JsonProperty("LastUpdatedDate")
    private String LastUpdatedDate;
    @JsonProperty("RecAreaID")
    private Integer RecAreaID;
    @JsonProperty("RecAreaLongitude")
    private Double RecAreaLongitude;
    @JsonProperty("RecAreaDirections")
    private String RecAreaDirections;
    @JsonProperty("RecAreaPhone")
    private String RecAreaPhone;
    @JsonProperty("OrgRecAreaID")
    private String OrgRecAreaID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return The RecAreaMapURL
     */
    @JsonProperty("RecAreaMapURL")
    public String getRecAreaMapURL() {
	return RecAreaMapURL;
    }

    /**
     *
     * @param RecAreaMapURL The RecAreaMapURL
     */
    @JsonProperty("RecAreaMapURL")
    public void setRecAreaMapURL(String RecAreaMapURL) {
	this.RecAreaMapURL = RecAreaMapURL;
    }

    public RecreationArea withRecAreaMapURL(String RecAreaMapURL) {
	this.RecAreaMapURL = RecAreaMapURL;
	return this;
    }

    /**
     *
     * @return The LastUpdatedBy
     */
    @JsonProperty("LastUpdatedBy")
    public Integer getLastUpdatedBy() {
	return LastUpdatedBy;
    }

    /**
     *
     * @param LastUpdatedBy The LastUpdatedBy
     */
    @JsonProperty("LastUpdatedBy")
    public void setLastUpdatedBy(Integer LastUpdatedBy) {
	this.LastUpdatedBy = LastUpdatedBy;
    }

    public RecreationArea withLastUpdatedBy(Integer LastUpdatedBy) {
	this.LastUpdatedBy = LastUpdatedBy;
	return this;
    }

    /**
     *
     * @return The RecAreaReservationURL
     */
    @JsonProperty("RecAreaReservationURL")
    public String getRecAreaReservationURL() {
	return RecAreaReservationURL;
    }

    /**
     *
     * @param RecAreaReservationURL The RecAreaReservationURL
     */
    @JsonProperty("RecAreaReservationURL")
    public void setRecAreaReservationURL(String RecAreaReservationURL) {
	this.RecAreaReservationURL = RecAreaReservationURL;
    }

    public RecreationArea withRecAreaReservationURL(String RecAreaReservationURL) {
	this.RecAreaReservationURL = RecAreaReservationURL;
	return this;
    }

    /**
     *
     * @return The RecAreaFeeDescription
     */
    @JsonProperty("RecAreaFeeDescription")
    public String getRecAreaFeeDescription() {
	return RecAreaFeeDescription;
    }

    /**
     *
     * @param RecAreaFeeDescription The RecAreaFeeDescription
     */
    @JsonProperty("RecAreaFeeDescription")
    public void setRecAreaFeeDescription(String RecAreaFeeDescription) {
	this.RecAreaFeeDescription = RecAreaFeeDescription;
    }

    public RecreationArea withRecAreaFeeDescription(String RecAreaFeeDescription) {
	this.RecAreaFeeDescription = RecAreaFeeDescription;
	return this;
    }

    /**
     *
     * @return The RecAreaName
     */
    @JsonProperty("RecAreaName")
    public String getRecAreaName() {
	return RecAreaName;
    }

    /**
     *
     * @param RecAreaName The RecAreaName
     */
    @JsonProperty("RecAreaName")
    public void setRecAreaName(String RecAreaName) {
	this.RecAreaName = RecAreaName;
    }

    public RecreationArea withRecAreaName(String RecAreaName) {
	this.RecAreaName = RecAreaName;
	return this;
    }

    /**
     *
     * @return The RecAreaDescription
     */
    @JsonProperty("RecAreaDescription")
    public String getRecAreaDescription() {
	return RecAreaDescription;
    }

    /**
     *
     * @param RecAreaDescription The RecAreaDescription
     */
    @JsonProperty("RecAreaDescription")
    public void setRecAreaDescription(String RecAreaDescription) {
	this.RecAreaDescription = RecAreaDescription;
    }

    public RecreationArea withRecAreaDescription(String RecAreaDescription) {
	this.RecAreaDescription = RecAreaDescription;
	return this;
    }

    /**
     *
     * @return The Keywords
     */
    @JsonProperty("Keywords")
    public String getKeywords() {
	return Keywords;
    }

    /**
     *
     * @param Keywords The Keywords
     */
    @JsonProperty("Keywords")
    public void setKeywords(String Keywords) {
	this.Keywords = Keywords;
    }

    public RecreationArea withKeywords(String Keywords) {
	this.Keywords = Keywords;
	return this;
    }

    /**
     *
     * @return The RecAreaEmail
     */
    @JsonProperty("RecAreaEmail")
    public String getRecAreaEmail() {
	return RecAreaEmail;
    }

    /**
     *
     * @param RecAreaEmail The RecAreaEmail
     */
    @JsonProperty("RecAreaEmail")
    public void setRecAreaEmail(String RecAreaEmail) {
	this.RecAreaEmail = RecAreaEmail;
    }

    public RecreationArea withRecAreaEmail(String RecAreaEmail) {
	this.RecAreaEmail = RecAreaEmail;
	return this;
    }

    /**
     *
     * @return The RecAreaLatitude
     */
    @JsonProperty("RecAreaLatitude")
    public Double getRecAreaLatitude() {
	return RecAreaLatitude;
    }

    /**
     *
     * @param RecAreaLatitude The RecAreaLatitude
     */
    @JsonProperty("RecAreaLatitude")
    public void setRecAreaLatitude(Double RecAreaLatitude) {
	this.RecAreaLatitude = RecAreaLatitude;
    }

    public RecreationArea withRecAreaLatitude(Double RecAreaLatitude) {
	this.RecAreaLatitude = RecAreaLatitude;
	return this;
    }

    /**
     *
     * @return The StayLimit
     */
    @JsonProperty("StayLimit")
    public String getStayLimit() {
	return StayLimit;
    }

    /**
     *
     * @param StayLimit The StayLimit
     */
    @JsonProperty("StayLimit")
    public void setStayLimit(String StayLimit) {
	this.StayLimit = StayLimit;
    }

    public RecreationArea withStayLimit(String StayLimit) {
	this.StayLimit = StayLimit;
	return this;
    }

    /**
     *
     * @return The GEOJSON
     */
    @JsonProperty("GEOJSON")
    public com.a2a.ridb.models.GeoCoordinates getGEOJSON() {
	return GEOJSON;
    }

    /**
     *
     * @param GEOJSON The GEOJSON
     */
    @JsonProperty("GEOJSON")
    public void setGEOJSON(com.a2a.ridb.models.GeoCoordinates GEOJSON) {
	this.GEOJSON = GEOJSON;
    }

    public RecreationArea withGEOJSON(com.a2a.ridb.models.GeoCoordinates GEOJSON) {
	this.GEOJSON = GEOJSON;
	return this;
    }

    /**
     *
     * @return The LastUpdatedDate
     */
    @JsonProperty("LastUpdatedDate")
    public String getLastUpdatedDate() {
	return LastUpdatedDate;
    }

    /**
     *
     * @param LastUpdatedDate The LastUpdatedDate
     */
    @JsonProperty("LastUpdatedDate")
    public void setLastUpdatedDate(String LastUpdatedDate) {
	this.LastUpdatedDate = LastUpdatedDate;
    }

    public RecreationArea withLastUpdatedDate(String LastUpdatedDate) {
	this.LastUpdatedDate = LastUpdatedDate;
	return this;
    }

    /**
     *
     * @return The RecAreaID
     */
    @JsonProperty("RecAreaID")
    public Integer getRecAreaID() {
	return RecAreaID;
    }

    /**
     *
     * @param RecAreaID The RecAreaID
     */
    @JsonProperty("RecAreaID")
    public void setRecAreaID(Integer RecAreaID) {
	this.RecAreaID = RecAreaID;
    }

    public RecreationArea withRecAreaID(Integer RecAreaID) {
	this.RecAreaID = RecAreaID;
	return this;
    }

    /**
     *
     * @return The RecAreaLongitude
     */
    @JsonProperty("RecAreaLongitude")
    public Double getRecAreaLongitude() {
	return RecAreaLongitude;
    }

    /**
     *
     * @param RecAreaLongitude The RecAreaLongitude
     */
    @JsonProperty("RecAreaLongitude")
    public void setRecAreaLongitude(Double RecAreaLongitude) {
	this.RecAreaLongitude = RecAreaLongitude;
    }

    public RecreationArea withRecAreaLongitude(Double RecAreaLongitude) {
	this.RecAreaLongitude = RecAreaLongitude;
	return this;
    }

    /**
     *
     * @return The RecAreaDirections
     */
    @JsonProperty("RecAreaDirections")
    public String getRecAreaDirections() {
	return RecAreaDirections;
    }

    /**
     *
     * @param RecAreaDirections The RecAreaDirections
     */
    @JsonProperty("RecAreaDirections")
    public void setRecAreaDirections(String RecAreaDirections) {
	this.RecAreaDirections = RecAreaDirections;
    }

    public RecreationArea withRecAreaDirections(String RecAreaDirections) {
	this.RecAreaDirections = RecAreaDirections;
	return this;
    }

    /**
     *
     * @return The RecAreaPhone
     */
    @JsonProperty("RecAreaPhone")
    public String getRecAreaPhone() {
	return RecAreaPhone;
    }

    /**
     *
     * @param RecAreaPhone The RecAreaPhone
     */
    @JsonProperty("RecAreaPhone")
    public void setRecAreaPhone(String RecAreaPhone) {
	this.RecAreaPhone = RecAreaPhone;
    }

    public RecreationArea withRecAreaPhone(String RecAreaPhone) {
	this.RecAreaPhone = RecAreaPhone;
	return this;
    }

    /**
     *
     * @return The OrgRecAreaID
     */
    @JsonProperty("OrgRecAreaID")
    public String getOrgRecAreaID() {
	return OrgRecAreaID;
    }

    /**
     *
     * @param OrgRecAreaID The OrgRecAreaID
     */
    @JsonProperty("OrgRecAreaID")
    public void setOrgRecAreaID(String OrgRecAreaID) {
	this.OrgRecAreaID = OrgRecAreaID;
    }

    public RecreationArea withOrgRecAreaID(String OrgRecAreaID) {
	this.OrgRecAreaID = OrgRecAreaID;
	return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
	return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
    }

    public RecreationArea withAdditionalProperty(String name, Object value) {
	this.additionalProperties.put(name, value);
	return this;
    }

    @Override
    public int hashCode() {
	return Pojomatic.hashCode(this);

    }

    @Override
    public boolean equals(Object object) {
	return Pojomatic.equals(this, object);
    }

    @Override
    public String toString() {
	return Pojomatic.toString(this);
    }

}
