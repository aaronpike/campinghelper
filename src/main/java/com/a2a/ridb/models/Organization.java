package com.a2a.ridb.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import org.pojomatic.Pojomatic;

/**
 * RIDB Organization Data
 * 
 * {
 * "OrgAbbrevName": "NPS", 
 * "OrgJurisdictionType": "Federal", 
 * "OrgType": "Department of the Interior", 
 * "LastUpdatedDate": "2007-02-26", 
 * "OrgURLText": "", 
 * "OrgURLAddress": "http://www.nps.gov", 
 * "OrgImageURL": "nps.gif",
 * "OrgParentID": 139, 
 * "OrgID": 128, 
 * "OrgName": "National Park Service" 
 * }
 */
public class Organization implements Serializable {
    
    @JsonProperty("OrgID")
    private int orgId;

    @JsonProperty("OrgParentID")
    private int orgParentId;

    @JsonProperty("OrgName")
    private String orgName;

    @JsonProperty("OrgAbbrevName")
    private String orgAbbrevName;

    @JsonProperty("OrgJurisdictionType")
    private String orgJurisdictionType;

    @JsonProperty("OrgType")
    private String orgType;
    
    @JsonProperty("LastUpdatedDate")
    private String lastUpdatedDate;

    @JsonProperty("OrgURLText")
    private String orgUrlText;

    @JsonProperty("OrgURLAddress")
    private String orgUrlAddress;

    @JsonProperty("OrgImageURL")
    private String orgImageUrl;

    /**
     * @return the orgId
     */
    public int getOrgId() {
	return orgId;
    }

    /**
     * @param orgId the orgId to set
     */
    public void setOrgId(int orgId) {
	this.orgId = orgId;
    }

    /**
     * @return the orgParentId
     */
    public int getOrgParentId() {
	return orgParentId;
    }

    /**
     * @param orgParentId the orgParentId to set
     */
    public void setOrgParentId(int orgParentId) {
	this.orgParentId = orgParentId;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
	return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
	this.orgName = orgName;
    }

    /**
     * @return the orgAbbrevName
     */
    public String getOrgAbbrevName() {
	return orgAbbrevName;
    }

    /**
     * @param orgAbbrevName the orgAbbrevName to set
     */
    public void setOrgAbbrevName(String orgAbbrevName) {
	this.orgAbbrevName = orgAbbrevName;
    }

    /**
     * @return the orgJurisdictionType
     */
    public String getOrgJurisdictionType() {
	return orgJurisdictionType;
    }

    /**
     * @param orgJurisdictionType the orgJurisdictionType to set
     */
    public void setOrgJurisdictionType(String orgJurisdictionType) {
	this.orgJurisdictionType = orgJurisdictionType;
    }

    /**
     * @return the orgType
     */
    public String getOrgType() {
	return orgType;
    }

    /**
     * @param orgType the orgType to set
     */
    public void setOrgType(String orgType) {
	this.orgType = orgType;
    }

    /**
     * @return the lastUpdatedDate
     */
    public String getLastUpdatedDate() {
	return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate the lastUpdatedDate to set
     */
    public void setLastUpdatedDate(String lastUpdatedDate) {
	this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return the orgUrlText
     */
    public String getOrgUrlText() {
	return orgUrlText;
    }

    /**
     * @param orgUrlText the orgUrlText to set
     */
    public void setOrgUrlText(String orgUrlText) {
	this.orgUrlText = orgUrlText;
    }

    /**
     * @return the orgUrlAddress
     */
    public String getOrgUrlAddress() {
	return orgUrlAddress;
    }

    /**
     * @param orgUrlAddress the orgUrlAddress to set
     */
    public void setOrgUrlAddress(String orgUrlAddress) {
	this.orgUrlAddress = orgUrlAddress;
    }

    /**
     * @return the orgImageUrl
     */
    public String getOrgImageUrl() {
	return orgImageUrl;
    }

    /**
     * @param orgImageUrl the orgImageUrl to set
     */
    public void setOrgImageUrl(String orgImageUrl) {
	this.orgImageUrl = orgImageUrl;
    }

    @Override
    public boolean equals(Object o) {
	return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
	return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
	return Pojomatic.toString(this);
    }

}
