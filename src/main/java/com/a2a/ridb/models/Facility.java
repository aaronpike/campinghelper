
package com.a2a.ridb.models;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "FacilityEmail",
    "FacilityLongitude",
    "FacilityDescription",
    "FacilityLatitude",
    "FacilityTypeDescription",
    "FacilityPhone",
    "FacilityMapURL",
    "FacilityReservationURL",
    "FacilityDirections",
    "FacilityName",
    "Keywords",
    "FacilityUseFeeDescription",
    "StayLimit",
    "GEOJSON",
    "LastUpdatedDate",
    "FacilityAdaAccess",
    "LegacyFacilityID",
    "OrgFacilityID",
    "FacilityID"
})
public class Facility {

    @JsonProperty("FacilityEmail")
    private String FacilityEmail;
    @JsonProperty("FacilityLongitude")
    private Double FacilityLongitude;
    @JsonProperty("FacilityDescription")
    private String FacilityDescription;
    @JsonProperty("FacilityLatitude")
    private Double FacilityLatitude;
    @JsonProperty("FacilityTypeDescription")
    private String FacilityTypeDescription;
    @JsonProperty("FacilityPhone")
    private String FacilityPhone;
    @JsonProperty("FacilityMapURL")
    private String FacilityMapURL;
    @JsonProperty("FacilityReservationURL")
    private String FacilityReservationURL;
    @JsonProperty("FacilityDirections")
    private String FacilityDirections;
    @JsonProperty("FacilityName")
    private String FacilityName;
    @JsonProperty("Keywords")
    private String Keywords;
    @JsonProperty("FacilityUseFeeDescription")
    private String FacilityUseFeeDescription;
    @JsonProperty("StayLimit")
    private String StayLimit;
    @JsonProperty("GEOJSON")
    private com.a2a.ridb.models.GeoCoordinates GEOJSON;
    @JsonProperty("LastUpdatedDate")
    private String LastUpdatedDate;
    @JsonProperty("FacilityAdaAccess")
    private String FacilityAdaAccess;
    @JsonProperty("LegacyFacilityID")
    private String LegacyFacilityID;
    @JsonProperty("OrgFacilityID")
    private String OrgFacilityID;
    @JsonProperty("FacilityID")
    private Integer FacilityID;
    @JsonIgnore
    private final Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * 
     * @return
     *     The FacilityEmail
     */
    @JsonProperty("FacilityEmail")
    public String getFacilityEmail() {
        return FacilityEmail;
    }

    /**
     * 
     * @param FacilityEmail
     *     The FacilityEmail
     */
    @JsonProperty("FacilityEmail")
    public void setFacilityEmail(String FacilityEmail) {
        this.FacilityEmail = FacilityEmail;
    }

    public Facility withFacilityEmail(String FacilityEmail) {
        this.FacilityEmail = FacilityEmail;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityLongitude
     */
    @JsonProperty("FacilityLongitude")
    public Double getFacilityLongitude() {
        return FacilityLongitude;
    }

    /**
     * 
     * @param FacilityLongitude
     *     The FacilityLongitude
     */
    @JsonProperty("FacilityLongitude")
    public void setFacilityLongitude(Double FacilityLongitude) {
        this.FacilityLongitude = FacilityLongitude;
    }

    public Facility withFacilityLongitude(Double FacilityLongitude) {
        this.FacilityLongitude = FacilityLongitude;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityDescription
     */
    @JsonProperty("FacilityDescription")
    public String getFacilityDescription() {
        return FacilityDescription;
    }

    /**
     * 
     * @param FacilityDescription
     *     The FacilityDescription
     */
    @JsonProperty("FacilityDescription")
    public void setFacilityDescription(String FacilityDescription) {
        this.FacilityDescription = FacilityDescription;
    }

    public Facility withFacilityDescription(String FacilityDescription) {
        this.FacilityDescription = FacilityDescription;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityLatitude
     */
    @JsonProperty("FacilityLatitude")
    public Double getFacilityLatitude() {
        return FacilityLatitude;
    }

    /**
     * 
     * @param FacilityLatitude
     *     The FacilityLatitude
     */
    @JsonProperty("FacilityLatitude")
    public void setFacilityLatitude(Double FacilityLatitude) {
        this.FacilityLatitude = FacilityLatitude;
    }

    public Facility withFacilityLatitude(Double FacilityLatitude) {
        this.FacilityLatitude = FacilityLatitude;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityTypeDescription
     */
    @JsonProperty("FacilityTypeDescription")
    public String getFacilityTypeDescription() {
        return FacilityTypeDescription;
    }

    /**
     * 
     * @param FacilityTypeDescription
     *     The FacilityTypeDescription
     */
    @JsonProperty("FacilityTypeDescription")
    public void setFacilityTypeDescription(String FacilityTypeDescription) {
        this.FacilityTypeDescription = FacilityTypeDescription;
    }

    public Facility withFacilityTypeDescription(String FacilityTypeDescription) {
        this.FacilityTypeDescription = FacilityTypeDescription;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityPhone
     */
    @JsonProperty("FacilityPhone")
    public String getFacilityPhone() {
        return FacilityPhone;
    }

    /**
     * 
     * @param FacilityPhone
     *     The FacilityPhone
     */
    @JsonProperty("FacilityPhone")
    public void setFacilityPhone(String FacilityPhone) {
        this.FacilityPhone = FacilityPhone;
    }

    public Facility withFacilityPhone(String FacilityPhone) {
        this.FacilityPhone = FacilityPhone;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityMapURL
     */
    @JsonProperty("FacilityMapURL")
    public String getFacilityMapURL() {
        return FacilityMapURL;
    }

    /**
     * 
     * @param FacilityMapURL
     *     The FacilityMapURL
     */
    @JsonProperty("FacilityMapURL")
    public void setFacilityMapURL(String FacilityMapURL) {
        this.FacilityMapURL = FacilityMapURL;
    }

    public Facility withFacilityMapURL(String FacilityMapURL) {
        this.FacilityMapURL = FacilityMapURL;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityReservationURL
     */
    @JsonProperty("FacilityReservationURL")
    public String getFacilityReservationURL() {
        return FacilityReservationURL;
    }

    /**
     * 
     * @param FacilityReservationURL
     *     The FacilityReservationURL
     */
    @JsonProperty("FacilityReservationURL")
    public void setFacilityReservationURL(String FacilityReservationURL) {
        this.FacilityReservationURL = FacilityReservationURL;
    }

    public Facility withFacilityReservationURL(String FacilityReservationURL) {
        this.FacilityReservationURL = FacilityReservationURL;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityDirections
     */
    @JsonProperty("FacilityDirections")
    public String getFacilityDirections() {
        return FacilityDirections;
    }

    /**
     * 
     * @param FacilityDirections
     *     The FacilityDirections
     */
    @JsonProperty("FacilityDirections")
    public void setFacilityDirections(String FacilityDirections) {
        this.FacilityDirections = FacilityDirections;
    }

    public Facility withFacilityDirections(String FacilityDirections) {
        this.FacilityDirections = FacilityDirections;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityName
     */
    @JsonProperty("FacilityName")
    public String getFacilityName() {
        return FacilityName;
    }

    /**
     * 
     * @param FacilityName
     *     The FacilityName
     */
    @JsonProperty("FacilityName")
    public void setFacilityName(String FacilityName) {
        this.FacilityName = FacilityName;
    }

    public Facility withFacilityName(String FacilityName) {
        this.FacilityName = FacilityName;
        return this;
    }

    /**
     * 
     * @return
     *     The Keywords
     */
    @JsonProperty("Keywords")
    public String getKeywords() {
        return Keywords;
    }

    /**
     * 
     * @param Keywords
     *     The Keywords
     */
    @JsonProperty("Keywords")
    public void setKeywords(String Keywords) {
        this.Keywords = Keywords;
    }

    public Facility withKeywords(String Keywords) {
        this.Keywords = Keywords;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityUseFeeDescription
     */
    @JsonProperty("FacilityUseFeeDescription")
    public String getFacilityUseFeeDescription() {
        return FacilityUseFeeDescription;
    }

    /**
     * 
     * @param FacilityUseFeeDescription
     *     The FacilityUseFeeDescription
     */
    @JsonProperty("FacilityUseFeeDescription")
    public void setFacilityUseFeeDescription(String FacilityUseFeeDescription) {
        this.FacilityUseFeeDescription = FacilityUseFeeDescription;
    }

    public Facility withFacilityUseFeeDescription(String FacilityUseFeeDescription) {
        this.FacilityUseFeeDescription = FacilityUseFeeDescription;
        return this;
    }

    /**
     * 
     * @return
     *     The StayLimit
     */
    @JsonProperty("StayLimit")
    public String getStayLimit() {
        return StayLimit;
    }

    /**
     * 
     * @param StayLimit
     *     The StayLimit
     */
    @JsonProperty("StayLimit")
    public void setStayLimit(String StayLimit) {
        this.StayLimit = StayLimit;
    }

    public Facility withStayLimit(String StayLimit) {
        this.StayLimit = StayLimit;
        return this;
    }

    /**
     * 
     * @return
     *     The GEOJSON
     */
    @JsonProperty("GEOJSON")
    public com.a2a.ridb.models.GeoCoordinates getGEOJSON() {
        return GEOJSON;
    }

    /**
     * 
     * @param GEOJSON
     *     The GEOJSON
     */
    @JsonProperty("GEOJSON")
    public void setGEOJSON(com.a2a.ridb.models.GeoCoordinates GEOJSON) {
        this.GEOJSON = GEOJSON;
    }

    public Facility withGEOJSON(com.a2a.ridb.models.GeoCoordinates GEOJSON) {
        this.GEOJSON = GEOJSON;
        return this;
    }

    /**
     * 
     * @return
     *     The LastUpdatedDate
     */
    @JsonProperty("LastUpdatedDate")
    public String getLastUpdatedDate() {
        return LastUpdatedDate;
    }

    /**
     * 
     * @param LastUpdatedDate
     *     The LastUpdatedDate
     */
    @JsonProperty("LastUpdatedDate")
    public void setLastUpdatedDate(String LastUpdatedDate) {
        this.LastUpdatedDate = LastUpdatedDate;
    }

    public Facility withLastUpdatedDate(String LastUpdatedDate) {
        this.LastUpdatedDate = LastUpdatedDate;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityAdaAccess
     */
    @JsonProperty("FacilityAdaAccess")
    public String getFacilityAdaAccess() {
        return FacilityAdaAccess;
    }

    /**
     * 
     * @param FacilityAdaAccess
     *     The FacilityAdaAccess
     */
    @JsonProperty("FacilityAdaAccess")
    public void setFacilityAdaAccess(String FacilityAdaAccess) {
        this.FacilityAdaAccess = FacilityAdaAccess;
    }

    public Facility withFacilityAdaAccess(String FacilityAdaAccess) {
        this.FacilityAdaAccess = FacilityAdaAccess;
        return this;
    }

    /**
     * 
     * @return
     *     The LegacyFacilityID
     */
    @JsonProperty("LegacyFacilityID")
    public String getLegacyFacilityID() {
        return LegacyFacilityID;
    }

    /**
     * 
     * @param LegacyFacilityID
     *     The LegacyFacilityID
     */
    @JsonProperty("LegacyFacilityID")
    public void setLegacyFacilityID(String LegacyFacilityID) {
        this.LegacyFacilityID = LegacyFacilityID;
    }

    public Facility withLegacyFacilityID(String LegacyFacilityID) {
        this.LegacyFacilityID = LegacyFacilityID;
        return this;
    }

    /**
     * 
     * @return
     *     The OrgFacilityID
     */
    @JsonProperty("OrgFacilityID")
    public String getOrgFacilityID() {
        return OrgFacilityID;
    }

    /**
     * 
     * @param OrgFacilityID
     *     The OrgFacilityID
     */
    @JsonProperty("OrgFacilityID")
    public void setOrgFacilityID(String OrgFacilityID) {
        this.OrgFacilityID = OrgFacilityID;
    }

    public Facility withOrgFacilityID(String OrgFacilityID) {
        this.OrgFacilityID = OrgFacilityID;
        return this;
    }

    /**
     * 
     * @return
     *     The FacilityID
     */
    @JsonProperty("FacilityID")
    public Integer getFacilityID() {
        return FacilityID;
    }

    /**
     * 
     * @param FacilityID
     *     The FacilityID
     */
    @JsonProperty("FacilityID")
    public void setFacilityID(Integer FacilityID) {
        this.FacilityID = FacilityID;
    }

    public Facility withFacilityID(Integer FacilityID) {
        this.FacilityID = FacilityID;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Facility withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
