package com.a2a.ridb;

import com.a2a.ridb.models.RecData;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class RidbClient {

    private final static Logger logger = LoggerFactory.getLogger(RidbClient.class);

    private final static String BASE_URI = "https://ridb.recreation.gov/api/v1/";
    private final static String APIKEY = "1092F1597A74433CA0FB5F2B1B0121CA";

    public RecData getFacilities(String latitude, String longitude, String radius) {
	Client client = ClientBuilder.newBuilder()
		.register(JacksonFeature.class)
		.build();
	
	Response response = client.target(RidbClient.BASE_URI)
		.path("facilities")
		.queryParam("latitude", latitude)
		.queryParam("longitude", longitude)
		.queryParam("radius", radius)
		.request(MediaType.APPLICATION_JSON)
		.header("apikey", APIKEY)		
		.get(Response.class);
	
	RecData recData = response.readEntity(RecData.class);

	return recData;
    }
}
